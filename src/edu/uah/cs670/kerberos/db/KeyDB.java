/**
 * 
 */
package edu.uah.cs670.kerberos.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStore.SecretKeyEntry;

import javax.crypto.spec.SecretKeySpec;

/**
 * Utility class that provides functionality for generating/storing/accessing keys
 * for clients/application servers and KDC.
 */

/**
 * @author shreedhan
 * 
 */

public class KeyDB {
	private String		dbFileName;
	private KeyStore	ks;
	private char[]		password;

	public KeyDB(String dbFileName) {
		this.dbFileName = System.getProperty("user.home") + "/" + dbFileName;
		password = "1234567890!@#$%^&*()[]{}||;:<>,./???".toCharArray();
	}

	private void loadKeyStore() {
		FileInputStream fis = getDBFileInputStream();
		try {
			ks = KeyStore.getInstance("JCEKS");
			ks.load(fis, password);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (fis != null)
				try {
					fis.close();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
		}

	}

	public SecretKeySpec getKey(String entityName) {
		loadKeyStore();
		try {
			KeyStore.SecretKeyEntry skEntry = (SecretKeyEntry) ks.getEntry(entityName, new KeyStore.PasswordProtection(
				password));
			return (SecretKeySpec) skEntry.getSecretKey();
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public void storeKey(String entityName, SecretKeySpec key) {
		loadKeyStore();
		FileOutputStream fos = getDBFileOutputStream();
		try {
			KeyStore.SecretKeyEntry skEntry = new KeyStore.SecretKeyEntry(key);
			ks.setEntry(entityName, skEntry, new KeyStore.PasswordProtection(password));
			ks.store(fos, password);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (fos != null) {
				try {
					fos.close();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private FileInputStream getDBFileInputStream() {
		FileInputStream fis = null;
		File file = new File(dbFileName);
		if (!file.exists())
			return null;
		try {
			fis = new FileInputStream(dbFileName);
		}
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return fis;

	}

	private FileOutputStream getDBFileOutputStream() {
		FileOutputStream fos = null;
		File file = new File(dbFileName);
		if (!file.exists())
			try {
				file.createNewFile();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		try {
			fos = new FileOutputStream(dbFileName);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return fos;
	}
}
