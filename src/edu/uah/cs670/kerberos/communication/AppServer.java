package edu.uah.cs670.kerberos.communication;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.Date;

import javax.crypto.SealedObject;
import javax.crypto.spec.SecretKeySpec;

import edu.uah.cs670.kerberos.crypto.Crypto;
import edu.uah.cs670.kerberos.model.Authenticator;
import edu.uah.cs670.kerberos.model.MessageType;
import edu.uah.cs670.kerberos.model.ServiceMessage;
import edu.uah.cs670.kerberos.model.ServiceResponseMessage;
import edu.uah.cs670.kerberos.model.StringMessage;
import edu.uah.cs670.kerberos.model.Ticket;

/**
 * Echo server running on port 12000 that echos the message sent by client with current timestamp.
 */

/**
 * @author Suman
 *
 */
public class AppServer {

	private static int				PORT		= 12000;
	private static String			SERVICE		= "echo";
	private static SecretKeySpec	SERVER_KEY	= Crypto.getKeyInDB("echo.jks", SERVICE);
	private static String			SESSION		= SERVICE + "_session.jks";
	private static SecretKeySpec	clientAppSessionKey;

	public static void main(String[] args) {
		System.out.println(getCurrentTimestamp() + " -- The echo server is running.");
		ServerSocket listener = null;
		try {
			listener = new ServerSocket(PORT);
			while (true) {
				new ClientThread(listener.accept()).start();
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				listener.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * Thread for handling each client request
	 */
	public static class ClientThread extends Thread {

		private Socket				clientSocket;
		private ObjectInputStream	in;
		private ObjectOutputStream	out;

		public ClientThread(Socket socket) {
			this.clientSocket = socket;
		}

		/*
		 * Listens for client request and give response accordingly.
		 * 
		 * @see java.lang.Thread#run()
		 */
		public void run() {
			try {
				in = new ObjectInputStream(clientSocket.getInputStream());
				out = new ObjectOutputStream(clientSocket.getOutputStream());

				Object request = in.readObject();
				if (request instanceof ServiceMessage) {
					ServiceMessage requestFromClient = (ServiceMessage) request;
					String displayMsg = getCurrentTimestamp() + " -- "
						+ requestFromClient.getType().getMessageTypeCode() + " request from client: "
						+ requestFromClient.getSenderName();
					System.out.println(displayMsg);
					SecretKeySpec clientSessionKey = Crypto.getKeyInDB(SESSION, requestFromClient.getClientName());

					SealedObject sealedAuthenticator = requestFromClient.getAuthenticator();
					SealedObject sealedTicket = requestFromClient.getServiceTicket();
					Authenticator authenticator = (Authenticator) Crypto.decrypt(sealedAuthenticator, clientSessionKey);
					Ticket ticket = (Ticket) Crypto.decrypt(sealedTicket, SERVER_KEY);
					if (compare(authenticator, ticket)) {
						StringMessage response = new StringMessage();
						String msg = "Welcome to service: " + SERVICE;
						response.setContent(msg);
						response.setType(MessageType.SERVICERES);

						System.out.println(getCurrentTimestamp() + " -- Response to client: " + authenticator.getName()
							+ " -- " + msg);
						out.writeObject(response);
					}
					else {
						System.out.println(getCurrentTimestamp()
							+ " -- Client authenticator information and ticket infromation does not match.");
					}
				}
				else if (request instanceof SealedObject) {
					SealedObject sealedMessageFromKdc = (SealedObject) request;
					ServiceResponseMessage messageFromKDC = (ServiceResponseMessage) Crypto.decrypt(
						sealedMessageFromKdc, SERVICE);
					System.out.println(getCurrentTimestamp() + " -- " + messageFromKDC.getType().getMessageTypeCode()
						+ " request from server:" + messageFromKDC.getSenderName());
					Authenticator authenticator = messageFromKDC.getAuthenticator();
					clientAppSessionKey = messageFromKDC.getSessionKey();
					Crypto.storeKeyInDB(SESSION, authenticator.getName(), clientAppSessionKey);

					StringMessage response = new StringMessage();
					response.setContent("Thank You.");
					response.setType(MessageType.SERVICERES);
					response.setSenderName(SERVICE);
					response.setReceiverName("KDC");

					System.out.println(getCurrentTimestamp() + " -- " + response.getContent() + " to server: "
						+ response.getReceiverName());
					out.writeObject(response);
				}
				else if (request instanceof StringMessage) {
					StringMessage msg = (StringMessage) request;
					String clientName = msg.getSenderName();
					System.out.println(getCurrentTimestamp() + " -- " + msg.getContent() + " form client: "
						+ clientName);

					StringMessage response = new StringMessage();
					String res = " Received \"" + msg.getContent() + "\" on " + getCurrentTimestamp();
					response.setContent(res);
					response.setType(MessageType.SERVICERES);
					response.setCreatedts(getCurrentTimestamp());
					response.setSenderName(SERVICE);
					response.setReceiverName(clientName);
					System.out.println(getCurrentTimestamp() + " -- " + "Response to client: " + msg.getSenderName()
						+ " -- " + res);
					out.writeObject(response);
				}
				else {
					System.out.println(getCurrentTimestamp() + " -- Message not recognized.");
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}

		public boolean compare(Authenticator authenticator, Ticket ticket) {
			if (!authenticator.getName().equals(ticket.getIssuedTo())) {
				return false;
			}
			if (!ticket.getIssuedAs().equals(SERVICE)) {
				return false;
			}
			return true;
		}

	}

	private static Timestamp getCurrentTimestamp() {
		return new Timestamp(new Date().getTime());
	}
}
