package edu.uah.cs670.kerberos.communication;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.SealedObject;
import javax.crypto.spec.SecretKeySpec;

import edu.uah.cs670.kerberos.crypto.Crypto;
import edu.uah.cs670.kerberos.model.Authenticator;
import edu.uah.cs670.kerberos.model.MessageType;
import edu.uah.cs670.kerberos.model.ServiceRequestMessage;
import edu.uah.cs670.kerberos.model.ServiceResponseMessage;
import edu.uah.cs670.kerberos.model.StringMessage;
import edu.uah.cs670.kerberos.model.Ticket;
import edu.uah.cs670.kerberos.model.TicketMessage;

/**
 * KDC server running on port 20000. Consists of Authentication Server(AS) and Ticket Granting Server(TGS). Listens for
 * authentication request from client and grants ticket granting ticket(TGT) and Service Ticket for accessing particular
 * service.
 */

/**
 * @author Suman
 *
 */
public class KDCServer {
	private static int				PORT			= 20000;
	private static String			DB_FILE			= "kdc.jks";
	private static String			SESSION_FILE	= "session.jks";
	private static SecretKeySpec	KDC_KEY			= Crypto.getKeyInDB(DB_FILE, "kdc");

	public static void main(String[] args) {

		System.out.println(getCurrentTimestamp() + " -- The kdc server is running.");
		ServerSocket listener = null;
		try {
			listener = new ServerSocket(PORT);
			while (true) {
				new ClientThread(listener.accept()).start();
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				listener.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Thread class for handling each client request.
	 */
	public static class ClientThread extends Thread {
		private Socket					connection;
		private ObjectInputStream		in;
		private ObjectOutputStream		out;
		private static SecretKeySpec	clientKdcSessionKey;

		public ClientThread(Socket socket) {
			this.connection = socket;
		}

		/**
		 * Listens for incoming client request and communicates with that client.
		 */
		public void run() {
			try {
				in = new ObjectInputStream(connection.getInputStream());
				out = new ObjectOutputStream(connection.getOutputStream());

				Object requestFromClient = in.readObject();

				// Request from client to AS for authenticating username.
				if (requestFromClient instanceof StringMessage) {
					StringMessage request = (StringMessage) requestFromClient;
					String clientName = request.getContent();
					String displayMsg = getCurrentTimestamp() + " -- " + request.getType().getMessageTypeCode()
						+ ": Authentication request from client: " + clientName;
					System.out.println(displayMsg);

					SecretKeySpec clientKey = Crypto.getKeyInDB(DB_FILE, request.getContent());
					String passphrase = Crypto.randomPassphrase();
					clientKdcSessionKey = Crypto.getKey(passphrase);
					Crypto.storeKeyInDB(SESSION_FILE, clientName, clientKdcSessionKey);

					TicketMessage messageToClient = new TicketMessage();
					Timestamp timestamp = getCurrentTimestamp();
					Ticket tgt = generateTicket(clientName, "TGS");
					messageToClient.setSessionKey(clientKdcSessionKey);
					messageToClient.setSealedTicket(Crypto.encrypt(tgt, KDC_KEY));
					messageToClient.setType(MessageType.ASRES);
					messageToClient.setSenderName("AS");
					messageToClient.setSenderIP(connection.getLocalAddress().toString());
					messageToClient.setReceiverName(clientName);
					messageToClient.setReceiverIP(connection.getRemoteSocketAddress().toString());
					messageToClient.setCreatedts(timestamp);

					displayMsg = timestamp + " -- " + messageToClient.getType().getMessageTypeCode()
						+ ": reponse to client: " + clientName;
					System.out.println(displayMsg);

					SealedObject sealedMessageToClient = Crypto.encrypt(messageToClient, clientKey);
					out.writeObject(sealedMessageToClient);

				}
				// Request from client to TGS for ticket to access desired service.
				else if (requestFromClient instanceof SealedObject) {
					SealedObject request = (SealedObject) requestFromClient;
					ServiceRequestMessage serviceRequest = (ServiceRequestMessage) Crypto.decrypt(request,
						clientKdcSessionKey);
					Authenticator authenticator = serviceRequest.getAuthenticator();
					Timestamp timestamp = getCurrentTimestamp();
					String displayMsg = timestamp + " -- " + serviceRequest.getType().getMessageTypeCode()
						+ " from client: " + serviceRequest.getSenderName();
					System.out.println(displayMsg);
					SealedObject sealedTGT = serviceRequest.getSealedTicket();
					Ticket tgt = (Ticket) Crypto.decrypt(sealedTGT, KDC_KEY);

					if (compare(authenticator, tgt)) {
						System.out.println("Authenticated client");
						SecretKeySpec clientAppSessionKey = Crypto.getRandomKey();

						// Send message to app server
						communicateWithAppServer(clientAppSessionKey, authenticator);

						// Send message to client
						Ticket ticket = generateTicket(authenticator.getName(), authenticator.getService());
						SealedObject sealedTicket = Crypto.encrypt(ticket,
							Crypto.getKeyInDB(DB_FILE, authenticator.getService()));
						TicketMessage responseMessage = new TicketMessage();
						responseMessage.setSessionKey(clientAppSessionKey);
						responseMessage.setSealedTicket(sealedTicket);
						responseMessage.setType(MessageType.TGSRES);
						responseMessage.setCreatedts(getCurrentTimestamp());
						responseMessage.setReceiverIP(authenticator.getIp());
						responseMessage.setReceiverName(authenticator.getName());
						responseMessage.setSenderName("TGS");

						displayMsg = getCurrentTimestamp() + " -- " + responseMessage.getType().getMessageTypeCode()
							+ " response to client: " + responseMessage.getReceiverName();
						System.out.println(displayMsg);

						SecretKeySpec clientKey = Crypto.getKeyInDB(DB_FILE, authenticator.getName());
						SealedObject sealedMessageToClient = Crypto.encrypt(responseMessage, clientKey);
						out.writeObject(sealedMessageToClient);
					}
					else {
						StringMessage error = new StringMessage();
						error.setType(MessageType.ERROR);
						String content = "Client authenticator and ticket information does not match.";
						error.setContent(content);
						System.out.println(content);

						try {
							out.writeObject(error);
						}
						catch (IOException e1) {
							e1.printStackTrace();
						}
					}

				}
				else {
					System.out.println("Message couldnot be recognized.");
				}
			}
			// catch (IOException e) {
			// e.printStackTrace();
			// }
			// catch (ClassNotFoundException e) {
			// e.printStackTrace();
			// }
			catch (Exception e) {
				StringMessage error = new StringMessage();
				error.setType(MessageType.ERROR);
				error.setContent("User doesnot exist");

				try {
					out.writeObject(error);
				}
				catch (IOException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
		}

		// Send information of client to application server.
		public void communicateWithAppServer(SecretKeySpec sessionKey, Authenticator authenticator) throws Exception {

			Socket appServer = new Socket(InetAddress.getByName("localhost"), 12000);
			ObjectOutputStream out = new ObjectOutputStream(appServer.getOutputStream());
			ObjectInputStream in = new ObjectInputStream(appServer.getInputStream());

			Timestamp timestamp = getCurrentTimestamp();
			ServiceResponseMessage messageToAppServer = new ServiceResponseMessage();
			messageToAppServer.setSessionKey(sessionKey);
			messageToAppServer.setAuthenticator(authenticator);
			messageToAppServer.setType(MessageType.SERVICEREQ);
			messageToAppServer.setSenderName("KDC");
			messageToAppServer.setReceiverName("");
			messageToAppServer.setCreatedts(timestamp);

			System.out.println(timestamp + " -- " + messageToAppServer.getType().getMessageTypeCode()
				+ " request to app server");
			SealedObject sealedMessage = Crypto.encrypt(messageToAppServer, authenticator.getService());
			out.writeObject(sealedMessage);

			StringMessage responseFromAppServer = (StringMessage) in.readObject();
			System.out.println(getCurrentTimestamp() + " -- " + responseFromAppServer.getContent() + " from server: "
				+ responseFromAppServer.getSenderName());
			appServer.close();
		}

		// Check if the client has access to the ticket.
		public boolean compare(Authenticator authenticator, Ticket tgt) {
			Timestamp currentTime = new Timestamp(new Date().getTime());
			if (!authenticator.getName().equals(tgt.getIssuedTo())) {
				return false;
			}
			if (tgt.getValidTo().before(currentTime)) {
				return false;
			}
			return true;
		}
	}

	// Generate ticket to be sent to client.
	private static Ticket generateTicket(String clientName, String service) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		Timestamp validFrom = new Timestamp(cal.getTime().getTime());
		cal.add(Calendar.HOUR_OF_DAY, 24);
		Timestamp validTo = new Timestamp(cal.getTime().getTime());
		Ticket ticket = new Ticket(validFrom, validTo, "KDC", clientName, service);
		return ticket;
	}

	private static Timestamp getCurrentTimestamp() {
		return new Timestamp(new Date().getTime());
	}
}
