package edu.uah.cs670.kerberos.communication;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.Date;

import javax.crypto.SealedObject;
import javax.crypto.spec.SecretKeySpec;

import edu.uah.cs670.kerberos.crypto.Crypto;
import edu.uah.cs670.kerberos.model.Authenticator;
import edu.uah.cs670.kerberos.model.Message;
import edu.uah.cs670.kerberos.model.MessageType;
import edu.uah.cs670.kerberos.model.ServiceMessage;
import edu.uah.cs670.kerberos.model.ServiceRequestMessage;
import edu.uah.cs670.kerberos.model.StringMessage;
import edu.uah.cs670.kerberos.model.TicketMessage;
import edu.uah.cs670.kerberos.ui.ClientUI;

/**
 * Client class that handles communication between client and the Key Distribution Center (KDC) and/or Application
 * server.
 */

/**
 * @author Suman
 * 
 */
public class Client {

	private String			clientName;
	private String			passphrase;
	private String			kdcHost;
	private int				kdcPort;
	private String			serverHost;
	private int				serverPort;
	private String			serviceName;
	private SecretKeySpec	sessionKey;

	public Client(String clientname, String password, String kdchost, int kdcport, String serverhost, int serverport,
		String servicename) {
		this.clientName = clientname;
		this.passphrase = password;
		this.kdcHost = kdchost;
		this.kdcPort = kdcport;
		this.serverHost = serverhost;
		this.serverPort = serverport;
		this.serviceName = servicename;
	}

	// Create the socket connection to communicate with server.
	public Socket connect(String host, int port) {
		Socket socket = null;
		InetAddress address;
		try {
			// Obtain an address object of the server
			address = InetAddress.getByName(host);
			// Establish a socket connection
			socket = new Socket(address, port);
			return socket;
		}
		catch (UnknownHostException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	// Communicates with KDC server to authenticate client and get the ticket for accessing service.
	public void authenticate() {
		try {
			Socket connection = connect(kdcHost, kdcPort);

			ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
			ObjectInputStream in = new ObjectInputStream(connection.getInputStream());

			Message messageToAS = authenticationMessage(connection);

			String displayMsg = messageToAS.getType().getMessageTypeCode() + " request to server: "
				+ messageToAS.getReceiverName();
			System.out.println(getCurrentTimestamp() + " -- " + displayMsg);
			ClientUI.displayLog(displayMsg);

			out.writeObject(messageToAS);

			Object responseFromAS = in.readObject();
			if (responseFromAS instanceof StringMessage) {
				StringMessage errorMsg = (StringMessage) responseFromAS;
				ClientUI.displayLog(errorMsg.getType().getMessageTypeCode() + " response from server: "
					+ errorMsg.getContent());
				ClientUI.killClientInstance();
				return;
			}

			SealedObject response = (SealedObject) responseFromAS;
			TicketMessage tgt = (TicketMessage) Crypto.decrypt(response, passphrase);
			sessionKey = tgt.getSessionKey();
			SealedObject sealedTGT = tgt.getSealedTicket();
			displayMsg = tgt.getType().getMessageTypeCode() + " response from server: " + tgt.getSenderName();
			System.out.println(getCurrentTimestamp() + " -- " + displayMsg);
			ClientUI.displayLog(displayMsg);

			connection = connect(kdcHost, kdcPort);

			out = new ObjectOutputStream(connection.getOutputStream());
			in = new ObjectInputStream(connection.getInputStream());

			ServiceRequestMessage messageToTGS = new ServiceRequestMessage();
			messageToTGS.setType(MessageType.TGSREQ);
			messageToTGS.setSealedTicket(sealedTGT);
			messageToTGS.setSenderName(clientName);
			messageToTGS.setSenderIP(connection.getLocalAddress().toString());
			messageToTGS.setReceiverName("TGS");
			messageToTGS.setReceiverIP(connection.getRemoteSocketAddress().toString());
			messageToTGS.setCreatedts(new Timestamp(new Date().getTime()));

			Authenticator authenticator = generateAuthenticator(connection);
			messageToTGS.setAuthenticator(authenticator);
			SealedObject sealedMessageToTGS = Crypto.encrypt(messageToTGS, sessionKey);

			displayMsg = messageToTGS.getType().getMessageTypeCode() + " request to server: "
				+ messageToTGS.getReceiverName();
			System.out.println(getCurrentTimestamp() + " -- " + displayMsg);
			ClientUI.displayLog(displayMsg);

			out.writeObject(sealedMessageToTGS);
			Object responseFromTGS = in.readObject();
			if (responseFromTGS instanceof StringMessage) {
				StringMessage errorMsg = (StringMessage) responseFromTGS;
				ClientUI.displayLog(errorMsg.getType().getMessageTypeCode() + " response from server: "
					+ errorMsg.getContent());
				ClientUI.killClientInstance();
				return;
			}
			response = (SealedObject) responseFromTGS;
			TicketMessage tgsResponse = (TicketMessage) Crypto.decrypt(response, passphrase);
			SealedObject sealedTicket = tgsResponse.getSealedTicket();
			SecretKeySpec clientAppSessionKey = tgsResponse.getSessionKey();

			displayMsg = tgsResponse.getType().getMessageTypeCode() + " response from server: "
				+ tgsResponse.getSenderName();
			System.out.println(getCurrentTimestamp() + " -- " + displayMsg);
			ClientUI.displayLog(displayMsg);

			authenticator = generateAuthenticator(connection);

			ServiceMessage messageToAppServer = new ServiceMessage();
			messageToAppServer.setClientName(clientName);
			messageToAppServer.setAuthenticator(Crypto.encrypt(authenticator, clientAppSessionKey));
			messageToAppServer.setServiceTicket(sealedTicket);
			messageToAppServer.setType(MessageType.SERVICEREQ);
			messageToAppServer.setSenderName(clientName);
			messageToAppServer.setReceiverName(serviceName);
			messageToAppServer.setCreatedts(new Timestamp(new Date().getTime()));

			communicateWithAppServer(serverHost, serverPort, messageToAppServer);
		}
		catch (UnknownHostException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			ClientUI.displayLog("Wrong credentials");
			ClientUI.killClientInstance();
			e.printStackTrace();
		}
	}

	// Communicates with the application server using the ticket provided by the KDC.
	public void communicateWithAppServer(String host, int port, ServiceMessage message) throws UnknownHostException,
		IOException, ClassNotFoundException, InterruptedException {

		String displayMsg = message.getType().getMessageTypeCode() + " request to server: " + serviceName;
		ClientUI.displayLog(displayMsg);
		System.out.println(getCurrentTimestamp() + " -- " + displayMsg);
		Socket appServer = connect(host, port);
		ObjectOutputStream out = new ObjectOutputStream(appServer.getOutputStream());
		ObjectInputStream in = new ObjectInputStream(appServer.getInputStream());

		out.writeObject(message);

		StringMessage responseFromAppServer = (StringMessage) in.readObject();
		System.out.println(getCurrentTimestamp() + " -- " + serviceName.toUpperCase() + ": "
			+ responseFromAppServer.getContent());
		ClientUI.displayLog(serviceName.toUpperCase() + ": " + responseFromAppServer.getContent());

		ClientUI.displayLog("Communication channel established. Please type message to send to the server.");
		ClientUI.disable();
	}

	// Communicates with the application server once the ticket is verified.
	public void sendMessageToAppServer(String host, int port, String message) {

		try {

			Socket appServer = connect(host, port);
			ObjectOutputStream out = new ObjectOutputStream(appServer.getOutputStream());
			ObjectInputStream in = new ObjectInputStream(appServer.getInputStream());
			StringMessage msg = new StringMessage();
			msg.setContent(message);
			msg.setType(MessageType.SERVICEREQ);
			msg.setSenderName(clientName);
			msg.setReceiverName(serviceName);
			msg.setCreatedts(new Timestamp(new Date().getTime()));
			out.writeObject(msg);

			StringMessage response = (StringMessage) in.readObject();
			System.out.println(getCurrentTimestamp() + " -- " + response.getContent());

			ClientUI.displayLog(serviceName.toUpperCase() + ": " + response.getContent());
		}
		catch (UnknownHostException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void close(Socket connection) {
		try {
			/** Close the socket connection. */
			connection.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * public static void main(String[] args) throws Exception { Client client = new Client("client", "client",
	 * "127.0.0.1", 20000, "localhost", 12000, "echo"); client.authenticate(); }
	 */

	// Generates initial authentication message to be sent to AS.
	public Message authenticationMessage(Socket socket) {
		StringMessage message = new StringMessage();
		message.setContent(clientName);
		message.setType(MessageType.ASREQ);
		message.setCreatedts(getCurrentTimestamp());
		message.setSenderIP(socket.getLocalAddress().toString());
		message.setSenderName(clientName);
		message.setReceiverIP(kdcHost);
		message.setReceiverName("AS");
		return message;
	}

	// Generates authenticator object for the client.
	public Authenticator generateAuthenticator(Socket socket) {
		Authenticator authenticator = new Authenticator();
		Timestamp timestamp = getCurrentTimestamp();
		authenticator.setIp(socket.getLocalAddress().toString());
		authenticator.setName(clientName);
		authenticator.setTimestamp(timestamp);
		authenticator.setService(serviceName);
		return authenticator;
	}

	private static Timestamp getCurrentTimestamp() {
		return new Timestamp(new Date().getTime());
	}
}
