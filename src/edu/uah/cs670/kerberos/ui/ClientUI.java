/**
 * 
 */
package edu.uah.cs670.kerberos.ui;

import java.sql.Timestamp;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import edu.uah.cs670.kerberos.communication.Client;

/**
 * GUI for entering the information about client and server to be connected and also shows the log of messages exchanges
 * between client and server.
 */

/**
 * @author Suman
 * 
 */
public class ClientUI {

	private static Text		logger			= null;
	private static Button	connectButton	= null;

	private static Client	client			= null;
	private static String	clientName		= null;
	private static String	password		= null;
	private static String	kdcAddress		= null;
	private static int		kdcPort			= 0;
	private static String	serviceName		= null;
	private static String	serviceAddress	= null;
	private static int		servicePort		= 0;

	private static String	CLIENT_NAME		= "Client Name";
	private static String	PASSWORD		= "Password";
	private static String	SERVICE_ADDRESS	= "Service Address";
	private static String	SERVICE_NAME	= "Service Name";
	private static String	SERVICE_PORT	= "Service Port";
	private static String	KDC_ADDRESS		= "KDC Address";
	private static String	KDC_PORT		= "KDC Port";
	private static String	CONNECT			= "Connect";
	private static String	SEND			= "Send";
	private static String	LOG				= "Log";

	public static void main(String[] args) {

		Display display = new Display();
		Shell shell = new Shell(display);
		GridLayout layout = new GridLayout(2, false);
		shell.setLayout(layout);

		Label clientNameLabel = new Label(shell, SWT.NONE);
		clientNameLabel.setText(CLIENT_NAME);

		final Text clientNameText = new Text(shell, SWT.BORDER);

		Label passwordLabel = new Label(shell, SWT.NONE);
		passwordLabel.setText(PASSWORD);

		final Text passwordText = new Text(shell, SWT.PASSWORD | SWT.BORDER);

		Label serviceNameLabel = new Label(shell, SWT.NONE);
		serviceNameLabel.setText(SERVICE_NAME);

		final Text serviceNameText = new Text(shell, SWT.BORDER);
		// serviceNameText.setText("echo");

		Label serviceAddressLabel = new Label(shell, SWT.NONE);
		serviceAddressLabel.setText(SERVICE_ADDRESS);

		final Text serviceAddressText = new Text(shell, SWT.BORDER);
		// serviceAddressText.setText("localhost");

		Label servicePortLabel = new Label(shell, SWT.NONE);
		servicePortLabel.setText(SERVICE_PORT);

		final Text servicePortText = new Text(shell, SWT.BORDER);
		// servicePortText.setText("12000");

		Label kdcAddressLabel = new Label(shell, SWT.NONE);
		kdcAddressLabel.setText(KDC_ADDRESS);

		final Text kdcAddressText = new Text(shell, SWT.BORDER);
		// kdcAddressText.setText("localhost");

		Label kdcPortLabel = new Label(shell, SWT.NONE);
		kdcPortLabel.setText(KDC_PORT);

		final Text kdcPortText = new Text(shell, SWT.BORDER);
		// kdcPortText.setText("20000");

		connectButton = new Button(shell, SWT.PUSH);
		connectButton.setText(CONNECT);
		connectButton.setToolTipText("Authenticate client to KDC");
		connectButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				super.widgetSelected(event);

				clientName = clientNameText.getText().toString();
				password = passwordText.getText().toString();

				serviceName = serviceNameText.getText().toString();
				serviceAddress = serviceAddressText.getText().toString();
				servicePort = Integer.parseInt(servicePortText.getText().toString());

				kdcAddress = kdcAddressText.getText().toString();
				kdcPort = Integer.parseInt(kdcPortText.getText().toString());

				ClientUI.displayLog("Authenticating to the KDC server...");
				getClient().authenticate();
			}
		});

		Group group = new Group(shell, SWT.NONE);
		group.setText(LOG);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData.horizontalSpan = 2;
		group.setLayoutData(gridData);
		group.setLayout(new GridLayout());

		logger = new Text(group, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.READ_ONLY);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalSpan = 2;
		gridData.heightHint = 200;
		logger.setLayoutData(gridData);

		final Text messageText = new Text(group, SWT.BORDER);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		messageText.setLayoutData(gridData);
		messageText.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent event) {
				if (event.keyCode == SWT.CR | event.keyCode == SWT.KEYPAD_CR) {
					final String message = messageText.getText().toString();
					displayLog(clientName.toUpperCase() + ": " + message);
					messageText.setText("");
					getClient().sendMessageToAppServer(serviceAddress, servicePort, message);
				}
			}
		});

		Button enterButton = new Button(shell, SWT.NONE);
		enterButton.setText(SEND);
		enterButton.setToolTipText("Send Message to Application Server");

		enterButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				final String message = messageText.getText().toString();
				displayLog(clientName.toUpperCase() + ": " + message);
				messageText.setText("");
				getClient().sendMessageToAppServer(serviceAddress, servicePort, message);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});

		shell.setSize(500, 600);
		shell.open();
		shell.setText("ClientUI");
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	public static void displayLog(String message) {

		String msg = logger.getText().toString();
		Timestamp timestamp = new Timestamp(new Date().getTime());
		message = timestamp + "-- " + message;
		if (msg.isEmpty()) {
			msg = message;
		}
		else {
			msg = msg + System.getProperty("line.separator") + message;
		}
		logger.setText(msg);
	}

	private static Client getClient() {
		if (client == null) {
			client = new Client(clientName, password, kdcAddress, kdcPort, serviceAddress, servicePort, serviceName);
		}
		return client;
	}

	public static void killClientInstance() {
		client = null;
	}

	public static void disable() {
		connectButton.setEnabled(false);
	}
}
