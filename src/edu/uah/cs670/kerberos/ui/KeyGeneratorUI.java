/**
 * 
 */
package edu.uah.cs670.kerberos.ui;

import javax.crypto.spec.SecretKeySpec;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import edu.uah.cs670.kerberos.crypto.Crypto;
import edu.uah.cs670.kerberos.db.KeyDB;

/**
 * User interface that allows user to enter information about the client and server to generates keys.
 */
/**
 * @author shreedhan
 * 
 */
public class KeyGeneratorUI {

	private static Text	logger;

	public static void main(String[] args) throws Exception {

		Display display = new Display();
		Shell shell = new Shell(display);

		GridLayout layout = new GridLayout(2, false);
		GridData gridData = null;
		shell.setLayout(layout);

		Label serverNameLabel = new Label(shell, SWT.NONE);
		serverNameLabel.setText("Server Name");
		final Text serverNameText = new Text(shell, SWT.BORDER);

		Label serverPasswordLabel = new Label(shell, SWT.NONE);
		serverPasswordLabel.setText("Server Password");
		final Text serverPasswordText = new Text(shell, SWT.BORDER);

		Label clientNameLabel = new Label(shell, SWT.NONE);
		clientNameLabel.setText("Client Name");
		final Text clientNameText = new Text(shell, SWT.BORDER);

		Label clientPasswordLabel = new Label(shell, SWT.NONE);
		clientPasswordLabel.setText("Client Password");
		final Text clientPasswordText = new Text(shell, SWT.BORDER);

		@SuppressWarnings("unused")
		Label spaceLabel = new Label(shell, SWT.NONE);
		Button button = new Button(shell, SWT.PUSH);
		button.setText("Generate keys");
		button.setToolTipText("Generate keys for KDC, Server and Client");

		button.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				super.widgetSelected(event);
				String serverName = serverNameText.getText();
				String serverPassword = serverPasswordText.getText();
				String clientName = clientNameText.getText();
				String clientPassword = clientPasswordText.getText();
				try {
					storeKeys(serverName, serverPassword, clientName, clientPassword);
					logger.setText(logger.getText() + "\n Generated keys successfully");
				}
				catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		logger = new Text(shell, SWT.MULTI | SWT.BORDER);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.heightHint = 100;
		gridData.horizontalSpan = 2;
		logger.setLayoutData(gridData);
		logger.setEnabled(false);

		shell.pack();
		shell.open();
		shell.setText("KeyGen");
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();

	}

	public static void storeKeys(String serverName, String serverPassword, String clientName, String clientPassword)
		throws Exception {

		SecretKeySpec kdcKey = Crypto.getKey("new password");
		KeyDB kdcKeyDB = new KeyDB("kdc.jks");
		kdcKeyDB.storeKey("kdc", kdcKey);

		SecretKeySpec clientKey = Crypto.getKey(clientPassword);
		SecretKeySpec serverKey = Crypto.getKey(serverPassword);
		KeyDB serverKeyDB = new KeyDB(serverName + ".jks");
		serverKeyDB.storeKey(serverName, serverKey);

		kdcKeyDB.storeKey(clientName, clientKey);
		kdcKeyDB.storeKey(serverName, serverKey);

	}

}
