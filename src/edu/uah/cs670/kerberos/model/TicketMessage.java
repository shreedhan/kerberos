/**
 * 
 */
package edu.uah.cs670.kerberos.model;

import java.io.Serializable;

import javax.crypto.SealedObject;
import javax.crypto.spec.SecretKeySpec;

/**
 * Models the message to be sent by Key Distribution Center (KDC) to the client, contains the session key and sealed
 * ticket object.
 */

/**
 * @author shreedhan
 * 
 */
public class TicketMessage extends Message implements Serializable {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -8422263821714279563L;
	private SecretKeySpec		sessionKey;
	private SealedObject		sealedTicket;

	/**
	 * @return the sessionKey
	 */
	public SecretKeySpec getSessionKey() {
		return sessionKey;
	}

	/**
	 * @param sessionKey
	 *            the sessionKey to set
	 */
	public void setSessionKey(SecretKeySpec sessionKey) {
		this.sessionKey = sessionKey;
	}

	/**
	 * @return the sealedTicket
	 */
	public SealedObject getSealedTicket() {
		return sealedTicket;
	}

	/**
	 * @param sealedTicket
	 *            the sealedTicket to set
	 */
	public void setSealedTicket(SealedObject sealedTicket) {
		this.sealedTicket = sealedTicket;
	}

}
