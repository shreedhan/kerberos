/**
 * 
 */
package edu.uah.cs670.kerberos.model;

import java.io.Serializable;

import javax.crypto.SealedObject;

/**
 * Models the message to be sent by client to Ticket Granting Server (TGS) asking for Service Ticket, contains the
 * sealed Ticket Granting Ticket (TGT) and the sealed authenticator.
 */

/**
 * @author shreedhan
 * 
 */
public class ServiceRequestMessage extends Message implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -2269511395872347105L;
	Authenticator				authenticator;
	SealedObject				sealedTicket;

	/**
	 * @return the authenticator
	 */
	public Authenticator getAuthenticator() {
		return authenticator;
	}

	/**
	 * @param authenticator
	 *            the authenticator to set
	 */
	public void setAuthenticator(Authenticator authenticator) {
		this.authenticator = authenticator;
	}

	/**
	 * @return the sealedTicket
	 */
	public SealedObject getSealedTicket() {
		return sealedTicket;
	}

	/**
	 * @param sealedTicket
	 *            the sealedTicket to set
	 */
	public void setSealedTicket(SealedObject sealedTicket) {
		this.sealedTicket = sealedTicket;
	}

}
