/**
 * 
 */
package edu.uah.cs670.kerberos.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Models the ticket object to be used used by Key Distribution center (KDC) as Ticket Granting Ticket (TGT) and Service
 * Ticket to sent to the client, contains the issuer of the ticket, to whom the ticket is issued, type of the ticket,
 * and the time ticket is valid for.
 */

/**
 * @author shreedhan
 * 
 */
public class Ticket implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 2922788765594633354L;
	private Timestamp			validFrom;
	private Timestamp			validTo;
	private String				issuedBy;
	private String				issuedTo;
	private String				issuedAs;

	public Ticket() {

	}

	public Ticket(Timestamp validFrom, Timestamp validTo, String issuedBy, String issuedTo, String issuedAs) {
		super();
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.issuedBy = issuedBy;
		this.issuedTo = issuedTo;
		this.issuedAs = issuedAs;
	}

	public Timestamp getValidFrom() {
		return validFrom;
	}

	public Timestamp getValidTo() {
		return validTo;
	}

	public String getIssuedBy() {
		return issuedBy;
	}

	public String getIssuedTo() {
		return issuedTo;
	}

	public String getIssuedAs() {
		return issuedAs;
	}

}
