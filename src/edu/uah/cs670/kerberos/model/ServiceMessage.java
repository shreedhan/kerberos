/**
 * 
 */
package edu.uah.cs670.kerberos.model;

import java.io.Serializable;

import javax.crypto.SealedObject;

/**
 * Models the message to be sent by client to the application server to confirm its authenticity to use desired service,
 * contains client name, sealed Service Ticket and sealed authenticator.
 */

/**
 * @author Suman
 *
 */
public class ServiceMessage extends Message implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 5704434739461081450L;

	private String				clientName;
	private SealedObject		serviceTicket;
	private SealedObject		authenticator;

	/**
	 * @return the clientName
	 */
	public String getClientName() {
		return clientName;
	}

	/**
	 * @param clientName
	 *            the clientName to set
	 */
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	/**
	 * @return the serviceTicket
	 */
	public SealedObject getServiceTicket() {
		return serviceTicket;
	}

	/**
	 * @param serviceTicket
	 *            the serviceTicket to set
	 */
	public void setServiceTicket(SealedObject serviceTicket) {
		this.serviceTicket = serviceTicket;
	}

	/**
	 * @return the authenticator
	 */
	public SealedObject getAuthenticator() {
		return authenticator;
	}

	/**
	 * @param authenticator
	 *            the authenticator to set
	 */
	public void setAuthenticator(SealedObject authenticator) {
		this.authenticator = authenticator;
	}
}
