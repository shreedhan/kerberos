/**
 * 
 */
package edu.uah.cs670.kerberos.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Base class for all types of messages to be used in communication between client, KDC and application server.
 */

/**
 * @author shreedhan
 * 
 */
public abstract class Message implements Serializable {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 5582700588900026938L;
	/**
	 * 
	 */

	private MessageType			type;
	private String				senderIP			= null;
	private String				senderName			= null;
	private String				receiverIP			= null;
	private String				receiverName		= null;
	private Timestamp			createdts			= new Timestamp(new Date().getTime());

	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public String getSenderIP() {
		return senderIP;
	}

	public void setSenderIP(String senderIP) {
		this.senderIP = senderIP;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getReceiverIP() {
		return receiverIP;
	}

	public void setReceiverIP(String receiverIP) {
		this.receiverIP = receiverIP;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public Timestamp getCreatedts() {
		return createdts;
	}

	public void setCreatedts(Timestamp createdts) {
		this.createdts = createdts;
	}

}
