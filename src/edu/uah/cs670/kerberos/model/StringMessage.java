/**
 * 
 */
package edu.uah.cs670.kerberos.model;

import java.io.Serializable;

/**
 * Simple form of message that contains strings as message content.
 */
/**
 * @author shreedhan
 * 
 */
public class StringMessage extends Message implements Serializable {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 3388852739751450648L;
	private String				content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
