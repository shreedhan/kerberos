/**
 * 
 */
package edu.uah.cs670.kerberos.model;

import java.io.Serializable;

import javax.crypto.spec.SecretKeySpec;

/**
 * Models the message to be sent by the Key Distribution Center (KDC) to the application server to notify about the
 * incoming client, contains the session key and authenticator object of the client.
 */

/**
 * @author Suman
 *
 */
public class ServiceResponseMessage extends Message implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -2136304705015769102L;

	private SecretKeySpec		sessionKey;
	private Authenticator		authenticator;

	/**
	 * @return the sessionKey
	 */
	public SecretKeySpec getSessionKey() {
		return sessionKey;
	}

	/**
	 * @param sessionKey
	 *            the sessionKey to set
	 */
	public void setSessionKey(SecretKeySpec sessionKey) {
		this.sessionKey = sessionKey;
	}

	/**
	 * @return the authenticator
	 */
	public Authenticator getAuthenticator() {
		return authenticator;
	}

	/**
	 * @param authenticator
	 *            the authenticator to set
	 */
	public void setAuthenticator(Authenticator authenticator) {
		this.authenticator = authenticator;
	}
}
