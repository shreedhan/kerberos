/**
 * 
 */
package edu.uah.cs670.kerberos.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Models the authenticator object of client and is sent by client to Key Distribution Center (KDC) and application
 * server to present its authenticity.
 */
/**
 * @author Suman
 *
 */
public class Authenticator implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 441022951684813278L;
	private String				name;
	private String				ip;
	private Timestamp			timestamp;
	private String				service;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip
	 *            the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the timestamp
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the service
	 */
	public String getService() {
		return service;
	}

	/**
	 * @param service
	 *            the service to set
	 */
	public void setService(String service) {
		this.service = service;
	}

}
