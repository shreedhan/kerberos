/**
 * 
 */
package edu.uah.cs670.kerberos.model;

/**
 * Enum class that represent different type of request/response used in messages.
 */

/**
 * @author shreedhan
 * 
 */
public enum MessageType {
	ASREQ("AS/REQ"), ASRES("AS/RES"), TGSREQ("TGS/REQ"), TGSRES("TGS/RES"), SERVICEREQ("APP/REQ"), SERVICERES("APP/RES"), ERROR(
		"AS/ERROR");

	private String	messageTypeCode;

	private MessageType(String messageType) {
		messageTypeCode = messageType;
	}

	public String getMessageTypeCode() {
		return messageTypeCode;
	}
}
