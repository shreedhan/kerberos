/**
 * 
 */
package edu.uah.cs670.kerberos.crypto;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import edu.uah.cs670.kerberos.db.KeyDB;
/**
 * Utility class that provides functionality for symmetric encryption/decryption of messages.
 */

/**
 * @author shreedhan
 * 
 */
public class Crypto {

	public static SecretKeySpec getKey(String passphrase) throws Exception {
		byte[] salt = "1234!@#!@#ASDFQWERZXCVWER{}?>!@#~~``~~#@".getBytes();
		int iterations = 10000;

		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		SecretKey tmp = factory.generateSecret(new PBEKeySpec(passphrase.toCharArray(), salt, iterations, 128));
		SecretKeySpec key = new SecretKeySpec(tmp.getEncoded(), "AES");
		return key;
	}

	public static SecretKeySpec getKeyInDB(String dbFileName, String entityName) {
		KeyDB keyDB = new KeyDB(dbFileName);
		SecretKeySpec entityKeyInKDB = keyDB.getKey(entityName);
		return entityKeyInKDB;

	}

	public static void storeKeyInDB(String dbFileName, String entityName,SecretKeySpec key){
		KeyDB keyDB = new KeyDB(dbFileName);
		keyDB.storeKey(entityName, key);
	}
	
	public static SealedObject encrypt(Object obj, String passphrase) throws Exception {
		SecretKeySpec key = getKey(passphrase);
		return encrypt(obj, key);
	}

	public static Object decrypt(SealedObject obj, String passphrase) throws Exception {
		SecretKeySpec key = getKey(passphrase);
		return decrypt(obj, key);
	}

	public static SealedObject encrypt(Object obj, SecretKeySpec key) throws Exception {
		Cipher cipher = Crypto.getEncryptCipher(key);
		SealedObject so = new SealedObject((Serializable) obj, cipher);
		return so;
	}

	public static Object decrypt(SealedObject so, SecretKeySpec key) throws Exception {
		Cipher cipher = Crypto.getDecryptCipher(key);
		Object obj = so.getObject(cipher);
		return obj;
	}

	public static Cipher getEncryptCipher(SecretKeySpec key) throws Exception {
		Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");
		aes.init(Cipher.ENCRYPT_MODE, key);
		return aes;
	}

	public static Cipher getDecryptCipher(SecretKeySpec key) throws Exception {
		Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");
		aes.init(Cipher.DECRYPT_MODE, key);
		return aes;
	}

	public static String randomPassphrase() {
		SecureRandom random = new SecureRandom();
		return new BigInteger(130, random).toString(32);
	}

	public static SecretKeySpec getRandomKey() throws Exception{
		return getKey(randomPassphrase());
	}
}
